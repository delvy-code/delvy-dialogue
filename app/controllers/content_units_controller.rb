class ContentUnitsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_content_unit, only: [:show]

  # GET /content_units/:id
  def show
    render json: @content_unit.to_json
  end

  private

  def set_content_unit
    @content_unit = ContentUnit.find(params[:id])
  end
end
