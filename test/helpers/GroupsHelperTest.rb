require 'test_helper'

class GroupsHelperTest < ActionView::TestCase
  include GroupsHelper

  test 'default_group_name_for_user should return the correct possessive name' do
    user = build(:user1, first_name: 'Lukas', last_name: 'Lukas')
    assert "Lukas Lukas's workspace", default_group_name_for_user(user)
  end
end
