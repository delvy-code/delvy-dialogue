class OriginalTextsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_original_text, only: [:show, :destroy]
  before_action :check_for_group, only: [:show]
  before_action :check_for_ownership, only: [:destroy]

  # GET /original_texts
  def index
    @own_original_texts = current_user.groups_as_owner.collect { |group| OriginalText.where(group_id: group.id) }.flatten
    @original_texts = current_user.groups_as_member.collect { |group| OriginalText.where(group_id: group.id) }.flatten
    render json: {own: @own_original_texts, all: @original_texts}
  end

  # POST /original_texts
  def create
    @author = current_user
    params_copy = original_text_import_params
    params_copy[:author_id] = @author.id
    @original_text_import = OriginalTextImport.create!(params_copy)
    ImportOriginalTextFromFileJob.perform_later @original_text_import
    render json: @original_text_import, status: :created
  end

  # GET /original_texts/:id
  def show
    @enriched_original_text = OpenStruct.new
    @enriched_original_text.original_text = @original_text
    @enriched_original_text.nodes = []

    xml_doc = Nokogiri::XML(@original_text.body, nil, Encoding::UTF_8.to_s)

    # TODO When there are "well-rated" change requests already, …
    # TODO - either add a property such as replaced_html to the enriched_content_unit
    # TODO - or append content_units' results (or even only the best-rated?) to them and handle in front-end

    content_units_by_nodes = @original_text.content_units.includes([:requests, :non_original_requests]).sort_by {|content_unit|
      content_unit.id
    }.group_by{|content_unit| content_unit.xpath_identifier}

    content_units_by_nodes.each do |xpath_identifier, content_units|
      node_type = xpath_identifier.gsub(/\/\/html\/body\//, "")
      node_type = node_type.gsub(/\[.*\]/, "")
      node = OpenStruct.new
      node.node_type = node_type
      node.content_units = content_units

      @enriched_original_text.nodes.push(node)
    end

    @enriched_original_text.nodes = @enriched_original_text.nodes.map(&:to_h).as_json
    render json: [@enriched_original_text].map(&:to_h).as_json[0]
  end

  # DELETE /original_texts/:id
  def destroy
    @original_text.destroy
    head :no_content
  end

  def set_original_text
    @original_text = OriginalText.find(params[:id])
  end

  def original_text_import_params
    params.require(:original_text).permit(:title, :original_file)
  end

  private

  def check_for_group
    head :unauthorized unless current_user.groups_as_member.include?(@original_text.group)
  end

  def check_for_ownership
    head :unauthorized unless current_user.groups_as_owner.include?(@original_text.group)
  end

end
