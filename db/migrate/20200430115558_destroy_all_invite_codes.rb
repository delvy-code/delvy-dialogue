class DestroyAllInviteCodes < ActiveRecord::Migration[5.2]
  def change
    InviteCode.destroy_all
  end
end
