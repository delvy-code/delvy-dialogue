--- Pandoc filter that replaces all Header elements (headings) with paragraphs
--- of their content, wrapped in a Strong tag
function Header(elem)
    return pandoc.Para{pandoc.Strong(elem.content)}
end
