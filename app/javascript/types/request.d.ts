interface IRequest {
    id?: number;
    reason?: string;
    content?: string;
    is_approved?: boolean;
    content_unit_id?: number;
    content_unit?: IContentUnit;
    author_id?: number;
    created_at?: string;
    updated_at?: string;
    author?: IUser;
    comments?: IComment[];
    ratings?: IRating[];
    worst_rating_value?: RatingValues;
    class_name?: string;
}