declare enum RatingValues {
    Veto = 0,
    Orange = 1,
    Yellow = 2,
    Green = 3
}

interface IRating extends IWithTimestamps {
    id?: number;
    content?: string;
    value?: RatingValues;
    author?: IUser;
    request?: IRequest;
}