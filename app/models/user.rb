class User < ApplicationRecord
  include GroupsHelper
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :requests
  has_many :comments
  has_many :ratings
  has_and_belongs_to_many :groups_as_member, class_name: "Group", join_table: 'groups_members', distinct: true
  has_and_belongs_to_many :groups_as_owner, class_name: "Group", join_table: 'groups_owners', distinct: true

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :first_name, uniqueness: { scope: :last_name }
  validates :sign_up_code,
            on: :create,
            inclusion: { in: proc { InviteCode.where(used: false).map(&:code) }, allow_blank: true, allow_nil: true, message: 'Invalid invite URL!' }

  validates_acceptance_of :constructive_consent, :allow_nil => false, :accept => "1", :on => :create
  validates_acceptance_of :privacy_consent, :allow_nil => false, :accept => "1", :on => :create
  validates_acceptance_of :personal_information_consent, :allow_nil => false, :accept => "1", :on => :create

  after_create :add_to_initial_group

  def add_to_initial_group
    if @sign_up_code && (invite_code = InviteCode.find_by(code: @sign_up_code)) && !invite_code.used
      # If user was registered using an invite code, add him/her to the corresponding group.
      group = Group.find_by(id: invite_code.group_id)
      group.members << self
      invite_code.update(used: true)
    else
      # Otherwise create a default group owned by the newly created user
      Group.create!(name: default_group_name_for_user(self), members: [self],
                    owners: [self])
    end
  end

  # Virtual attribute for authenticating by either name or email
  # This is in addition to a real persisted field like 'name'
  attr_accessor :login

  attr_accessor :sign_up_code

  def attributes
    super.merge(
        name: name
    )
  end

  def name
    return first_name if last_name.blank?
    first_name + " " + last_name
  end

  # TODO this only mocks a mapping from possibly multiple groups to one
  # TODO fix in the scope of https://gitlab.com/delvy-code/delvy-dialogue/-/issues/91
  def active_group
    groups_as_member[0]
  end

  def self.find_for_database_authentication warden_conditions
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    where(conditions).where(['lower(email) = :value', {value: login.strip.downcase}]).first
  end

  # Attempt to find a user by it's email. If a record is found, send new
  # password instructions to it. If not user is found, returns a new user
  # with an email not found error.
  def self.send_reset_password_instructions attributes = {}
    recoverable = find_recoverable_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    recoverable.send_reset_password_instructions if recoverable.persisted?
    recoverable
  end

  def self.find_recoverable_or_initialize_with_errors required_attributes, attributes, error = :invalid
    (case_insensitive_keys || []).each { |k| attributes[k].try(:downcase!) }

    attributes = attributes.slice(*required_attributes)
    attributes.delete_if { |_key, value| value.blank? }

    if attributes.keys.size == required_attributes.size
      if attributes.key?(:login)
        login = attributes.delete(:login)
        record = find_record(login)
      else
        record = where(attributes).first
      end
    end

    unless record
      record = new

      required_attributes.each do |key|
        value = attributes[key]
        record.send("#{key}=", value)
        record.errors.add(key, value.present? ? error : :blank)
      end
    end
    record
  end

  def self.find_record login
    where(['email = :value', { value: login }]).first
  end
end
