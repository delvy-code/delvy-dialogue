# Helper module for groups
module GroupsHelper
  # @param [User] user
  # @return [string] group name
  def default_group_name_for_user(user)
    "#{user.name.possessive} workspace"
  end
end
