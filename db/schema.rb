# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_10_180409) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "agreement_requests_requests", id: false, force: :cascade do |t|
    t.bigint "agreement_request_id"
    t.bigint "request_id"
    t.index ["agreement_request_id"], name: "index_agreement_requests_requests_on_agreement_request_id"
    t.index ["request_id"], name: "index_agreement_requests_requests_on_request_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "content"
    t.boolean "is_approved"
    t.bigint "author_id"
    t.bigint "request_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_comments_on_author_id"
    t.index ["request_id"], name: "index_comments_on_request_id"
  end

  create_table "content_units", force: :cascade do |t|
    t.integer "starts_at"
    t.integer "ends_at"
    t.boolean "should_render"
    t.bigint "original_text_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "xpath_identifier"
    t.string "original_content"
    t.index ["original_text_id"], name: "index_content_units_on_original_text_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups_members", id: false, force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "user_id", null: false
    t.index ["group_id", "user_id"], name: "by_group_and_member", unique: true
    t.index ["group_id"], name: "index_groups_members_on_group_id"
    t.index ["user_id"], name: "index_groups_members_on_user_id"
  end

  create_table "groups_owners", id: false, force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "user_id", null: false
    t.index ["group_id", "user_id"], name: "by_group_and_owner", unique: true
    t.index ["group_id"], name: "index_groups_owners_on_group_id"
    t.index ["user_id"], name: "index_groups_owners_on_user_id"
  end

  create_table "invite_codes", force: :cascade do |t|
    t.string "code"
    t.boolean "used"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "group_id"
    t.index ["group_id"], name: "index_invite_codes_on_group_id"
  end

  create_table "original_text_imports", force: :cascade do |t|
    t.string "title"
    t.bigint "original_text_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "author_id"
    t.index ["author_id"], name: "index_original_text_imports_on_author_id"
    t.index ["original_text_id"], name: "index_original_text_imports_on_original_text_id"
  end

  create_table "original_texts", force: :cascade do |t|
    t.string "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.integer "sorting_priority", default: 0
    t.boolean "hidden", default: false
    t.bigint "group_id"
    t.index ["group_id"], name: "index_original_texts_on_group_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.string "content"
    t.integer "value"
    t.bigint "author_id"
    t.bigint "request_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_ratings_on_author_id"
    t.index ["request_id"], name: "index_ratings_on_request_id"
  end

  create_table "requests", force: :cascade do |t|
    t.string "reason"
    t.string "content"
    t.boolean "is_approved"
    t.bigint "content_unit_id"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.index ["author_id"], name: "index_requests_on_author_id"
    t.index ["content_unit_id"], name: "index_requests_on_content_unit_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.boolean "is_moderator"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "last_name"
    t.string "organization"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["first_name", "last_name"], name: "index_users_on_first_name_and_last_name", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "comments", "requests"
  add_foreign_key "comments", "users", column: "author_id"
  add_foreign_key "content_units", "original_texts"
  add_foreign_key "invite_codes", "groups"
  add_foreign_key "original_text_imports", "original_texts"
  add_foreign_key "original_text_imports", "users", column: "author_id"
  add_foreign_key "original_texts", "groups"
  add_foreign_key "ratings", "requests"
  add_foreign_key "ratings", "users", column: "author_id"
  add_foreign_key "requests", "content_units"
  add_foreign_key "requests", "users", column: "author_id"
end
