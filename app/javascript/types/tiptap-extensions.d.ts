// Taken from https://gist.github.com/Chris2011/ae357032eca061995a80c05087f6c5d5
declare module "tiptap-extensions" {
    import Vue from "vue";

    export class Blockquote {}
    export class CodeBlock {}
    export class HardBreak {}
    export class Heading {}
    export class OrderedList {}
    export class BulletList {}
    export class ListItem {}
    export class TodoItem {}
    export class TodoList {}
    export class Bold {}
    export class Code {}
    export class Italic {}
    export class Link {}
    export class Strike {}
    export class Underline {}
    export class History {}
}
