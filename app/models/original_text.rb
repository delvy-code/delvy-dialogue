class OriginalText < ApplicationRecord
  has_one :original_text_import
  has_many :content_units, dependent: :destroy
  belongs_to :group
end
