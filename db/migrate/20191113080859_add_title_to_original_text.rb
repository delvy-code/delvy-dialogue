class AddTitleToOriginalText < ActiveRecord::Migration[5.2]
  def change
    change_table :original_texts do |t|
      t.string :title
    end
  end
end
