class CreateGroupsMembersTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :groups, :users, table_name: :groups_members do |t|
      # Create indices for query speedup, see https://stackoverflow.com/a/5120734/6852970
      t.index :group_id
      t.index :user_id
    end


  end
end
