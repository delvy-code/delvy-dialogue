interface IOriginalText {
    id: number;
    title: string;
    body: string;
    created_at: string;
    updated_at: string;
    sorting_priority: number;
    hidden: boolean;
}
