FactoryBot.define do
  factory :rating do
    content { 'TODO Remove' }
    author { build(:user2) }
    request

    factory :red_rating do
      value { Rating.readable_values[:red] }
    end

    factory :orange_rating do
      value { Rating.readable_values[:orange] }
    end

    factory :yellow_rating do
      value { Rating.readable_values[:yellow] }
    end

    factory :green_rating do
      value { Rating.readable_values[:green] }
    end
  end
end
