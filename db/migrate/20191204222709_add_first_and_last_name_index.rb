class AddFirstAndLastNameIndex < ActiveRecord::Migration[5.2]
  def change
    add_index :users, [:first_name, :last_name], unique: true
  end
end
