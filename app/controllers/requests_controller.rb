class RequestsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_request, only: [:show]

  # GET /content_unit/:content_unit_id/requests
  def index
    @content_unit = ContentUnit.find(params[:content_unit_id])
    @sorted_requests = @content_unit.sorted_requests
    render json: @sorted_requests.to_json(include: [:author,
                                                    {
                                                      comments: {
                                                        include: :author
                                                      }
                                                    },
                                                    {
                                                      ratings: {
                                                        include: :author
                                                      }
                                                    }],
                                          methods: [:class_name])
  end

  # POST /content_unit/:content_unit_id/requests
  def create
    @content_unit = ContentUnit.find(params[:content_unit_id])

    # Create an OriginalText containing the original data if this will be the first change request for the content unit.
    if @content_unit.amount_of_requests == 0
      OriginalRequest.create!(:is_approved => true, :author_id => "", :content_unit_id => @content_unit.id, :content =>  @content_unit.original_content)
    end

    @author = current_user
    request_params_copy = request_params
    # TODO Only set "is_approved" to true if review mode (to be implemented) is disabled
    request_params_copy[:is_approved] = true
    request_params_copy[:author_id] = @author.id
    request_params_copy[:content_unit_id] = @content_unit.id
    request_params_copy[:request_ids] = request_params_copy[:requests]

    # Create actual change request
    @request = if request_params_copy[:request_ids]
                 AgreementRequest.create!(request_params_copy.except(:requests))
               else
                 ChangeRequest.create!(request_params_copy.except(:request_ids))
               end

    render json: @request, status: :created
  end

  # GET /requests/:id
  def show
    include_array = [
      author: {},
      content_unit: {
        only: %i[
          original_content
          id
        ]
      },
      comments: {},
      ratings: {}
    ]
    if @request.instance_of? AgreementRequest
      include_array = include_array.push(requests: {include: include_array.map(&:clone), methods: [:class_name]})
    end
    render json: @request.to_json(include: include_array,
                                  methods: [:class_name])
  end

  private

  def request_params
    # whitelist params
    params.permit(:content, :reason, requests: [])
  end

  def set_request
    @request = Request.includes(:author, :content_unit, :comments, :ratings)
                      .find(params[:id])
  end
end
