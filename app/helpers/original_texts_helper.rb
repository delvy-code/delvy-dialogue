module OriginalTextsHelper
    def import_html(html_string, title, group_id, priority = 0)
        doc = Nokogiri::HTML(html_string)
        body = doc.xpath("//body").first
        content_units = []

        def matches(s, re)
          start_at = 0
          matches = []
          while (m = s.match(re, start_at))
            matches.push(m)
            start_at = m.end(0)
          end
          matches
        end

        body.element_children.each do |element|
          if element.name === "p"
            # Split the paragraph's content into sentences, leaving any possible footnotes (<a href=""...>) as is
            sentences = element.inner_html.split(/(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s/)
            sentences = sentences.map! { |sentence|
              # if matches = sentence.scan(/(?:[.?”])<a\b[^>]*>(?:.*?)<\/a>(?<space>\s)/)
              if (matches = matches(sentence, /(?:[.?”])<a\b[^>]*>(?:.*?)<\/a>(?<space>\s)/)).length > 0
                split_strings = []
                start_at = 0
                matches.each_with_index do |match, index|
                  sentence_copy = sentence.clone
                  if index == matches.size - 1
                    end_at = sentence.length - 1
                    split_strings << [sentence_copy[start_at..match.begin(:space) - 1], sentence_copy[match.begin(:space) + 1..end_at]]
                  else
                    end_at = match.begin(:space) - 1
                    split_strings << sentence_copy[start_at..end_at]
                  end
                  start_at = end_at + 1
                end
                split_strings
              else
                sentence
              end
            }.flatten

            sentences = sentences.map! { |sentence|
              Nokogiri::HTML::fragment(sentence)
            }

            # Prepare a replacement "p" element for putting the replaced footnote back into the whole document structure
            replaced_element = Nokogiri::XML::Node.new "p", doc

            # Replace all footnotes references with appropriate <footnote>...</footnote> elements
            sentences.each do |sentence|
              sentence.element_children.each do |child_element|
                if child_element.name === "a" and child_element["class"] === "footnote-ref"
                  # Remove the leading "#" character from the href
                  corresponding_footnote_id = child_element["href"][1..-1]
                  corresponding_footnote = doc.xpath("//*[@id='%s']" % corresponding_footnote_id)
                  footnote_content = corresponding_footnote.children.first
                  back_reference = footnote_content.search("//a[@class='footnote-back']")
                  back_reference.remove
                  replaced_footnote_element = Nokogiri::XML::Node.new "footnote", doc
                  replaced_footnote_element.children = footnote_content
                  child_element.replace(replaced_footnote_element)
                end
              end

              # Build up the replacement "p" element by putting each of the sentences (with possible replacements)
              # into the "p" element's children
              replaced_element.add_child sentence.clone
            end

            element = element.replace(replaced_element)

            # Build sentence objects for each of the sentences
            ending_index = 0
            sentences.each { |sentence_node|
              sentence = sentence_node.to_html
              starting_index = element.to_html.index(sentence, ending_index)
              ending_index = starting_index + sentence.length - 1
              xpath_identifier = Nokogiri::CSS.xpath_for element.css_path
              ending_index += 1
              content_units.push(ContentUnit.new(starts_at: starting_index, ends_at: ending_index, should_render: true,
                                                 xpath_identifier: xpath_identifier[0], original_content: sentence))
            }

          elsif element.name === "section" and element["class"] === "footnotes"
            # Remove section where the footnotes have been put by pandoc
            element.remove
          else
            # Do nothing with any other element (such as h1, h2 etc.)
            xpath_identifier = Nokogiri::CSS.xpath_for element.css_path
            content_units.push(ContentUnit.new(starts_at: nil, ends_at: nil, should_render: true, xpath_identifier: xpath_identifier[0]))
          end
        end

        original_text_body_xml = <<-XML
      <html>
      #{body}
      </html>
        XML

        original_text = OriginalText.create(
            body: original_text_body_xml,
            title: title,
            sorting_priority: priority,
            group_id: group_id
        )

        content_units.each do |content_unit|
          content_unit.original_text = original_text
          content_unit.save
        end

        # Return the ID of the original_text that has been created
        original_text.id
      end
end
