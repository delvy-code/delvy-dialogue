namespace :import_original_text do
  desc "Import an original text from a HTML document and split it into ContentUnits.
  Parameters:
    - Path to a to-be-imported HTML file (mandatory)
    - Title of the original text (mandatory)
    - ID of the group that owns the text (mandatory)
    - Numeric priority (optional, default: 0)"
  task :from_html, [:path_to_html_file, :title, :group_id, :priority] => :environment do |_, args_rake|
    _, *args = ARGV

    args_rake.with_defaults(:priority => 0)

    path_to_html_file = args_rake.path_to_html_file || args.first
    title = args_rake.title || args.second
    if args.third
      priority = Integer(args.third)
    end
    priority ||= args_rake.priority

    abort("Aborted: Need file path and title as parameters") unless path_to_html_file && title

    if File.file?(path_to_html_file)
      # A path to a to-be-imported HTML file has been passed
      path_to_html_file = path_to_html_file
      doc = File.read(path_to_html_file)
    else
      abort("Aborted: There exists no file at the given path")
    end
  
    ApplicationController.helpers.import_html(doc, title, group_id, priority)
  end
end
