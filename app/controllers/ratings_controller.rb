class RatingsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  # GET /requests/:request_id/ratings
  def index
    @ratings = Rating.includes(:author)
                     .where(request_id: params[:request_id])
    render json: @ratings.to_json(include: [:author])
  end

  # POST /requests/:request_id/ratings
  def create
    @request = Request.find(params[:request_id])
    @author = current_user

    # If there already exists a rating from the same user, update it (or delete it on value === null)
    existing_rating_by_same_user = Rating.where(request_id: params[:request_id],
                                                author_id: @author.id)
                                         .first
    if existing_rating_by_same_user
      # If the rating's value is set to "null", remove the rating
      if request_params[:value].blank?
        existing_rating_by_same_user.destroy!
        render json: [], status: :accepted
        return
      end
      existing_rating_by_same_user.value = request_params[:value]
      existing_rating_by_same_user.save!
      render json: existing_rating_by_same_user, status: :created
      return
    end

    request_params_copy = request_params
    request_params_copy[:author_id] = @author.id
    request_params_copy[:request_id] = @request.id
    @rating = Rating.create!(request_params_copy)
    render json: @rating, status: :created
  end

  private

  def request_params
    # whitelist params
    params.require(:rating).permit(:value)
  end
end
