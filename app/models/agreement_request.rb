class AgreementRequest < Request
  has_and_belongs_to_many :requests, join_table: 'agreement_requests_requests'
end
