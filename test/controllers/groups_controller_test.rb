require 'test_helper'

class GroupsControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  setup do
    @group = create(:group)
    @user = create(:user1)
  end

  test 'should join group when logged in' do
    login_as @user
    code = create(:valid_invite_code, group_id: @group.id)
    assert_not_includes @user.groups_as_member, @group
    get join_group_url(@group, invite_code: code.code)
    assert_includes @user.groups_as_member, @group
    assert_redirected_to(root_path)
  end

  test 'should not join group with invalid group' do
    login_as @user
    code = create(:invalid_invite_code, group_id: @group.id)
    assert_not_includes @user.groups_as_member, @group
    get join_group_url(@group, invite_code: code.code)
    assert_not_includes @user.groups_as_member, @group
    assert_redirected_to(root_path)
  end

  test 'should redirect to register page when not logged in' do
    code = create(:valid_invite_code, group_id: @group.id)
    get join_group_url(@group, invite_code: code.code)
    assert_redirected_to(new_user_registration_path + "?invite_code=#{code.code}")
  end

  test 'should invite user' do
    login_as @user
    test_email = 'test@test.com'
    message_delivery = mock('ActionMailer::MessageDelivery')
    message_delivery.responds_like_instance_of(ActionMailer::MessageDelivery)
    message_delivery.expects(:deliver_later)
    UserNotifierMailer.expects(:send_invite_email).returns(message_delivery)
    @group.members << @user
    post invite_user_group_url(@group), params: {'groups_invite_user[email]': test_email}

    assert_response :success
  end
end
