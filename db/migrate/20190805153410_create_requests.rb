class CreateRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.string :reason
      t.string :content
      t.boolean :is_approved

      t.belongs_to :content_unit, index: true, foreign_key: true
      t.belongs_to :author, index: true, foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
