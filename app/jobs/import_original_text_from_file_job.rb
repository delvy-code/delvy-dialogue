require 'pandoc-ruby'

class ImportOriginalTextFromFileJob < ApplicationJob
  queue_as :default

  def perform(original_text_import)
    ActionCable.server.broadcast "import_original_text_user_#{original_text_import.author.id}",
                                 status: "IMPORT_STARTED"

    # TODO: Starting with Rails 6.0, we can use ActiveStorage::Blob::open
    # TODO: (see https://github.com/rails/rails/blob/6-0-stable/activestorage/app/models/active_storage/blob.rb#L218)
    # TODO: Use as follows: `original_text_import.original_file.open do |file|`

    original_file = original_text_import.original_file
    filename = original_file.filename.to_s
    filename_cleaned = filename.split('.').map(&:parameterize).join('.')
    file_path = "#{Dir.tmpdir}/#{filename_cleaned}"

    # Temporarily download the file from ActiveStorage to a local directory
    File.open(file_path, 'wb') do |file|
      file.write(original_file.download)
    end

    # Convert the downloaded file to HTML using Pandoc
    begin
      html = PandocRuby.new([file_path],
                            from: 'docx')
                 .to_html("--lua-filter=#{Rails.root.join( "lib/pandoc-filters/replace-blockquotes-with-content.lua" )}",
                          "--lua-filter=#{Rails.root.join( "lib/pandoc-filters/replace-lists-with-list-items-contents.lua" )}",
                          "--lua-filter=#{Rails.root.join( "lib/pandoc-filters/replace-headers-with-strong-paragraphs.lua" )}",
                          "--lua-filter=#{Rails.root.join( "lib/pandoc-filters/remove-tables.lua" )}",
                          "--lua-filter=#{Rails.root.join( "lib/pandoc-filters/remove-images.lua" )}",
                          "--lua-filter=#{Rails.root.join( "lib/pandoc-filters/replace-hyperlinks-with-text-content.lua" )}")
    rescue StandardError => e
      ActionCable.server.broadcast "import_original_text_user_#{original_text_import.author.id}",
                                   status: "IMPORT_FAILED",
                                   error_message: "The document could not be read. Please make sure that it is a valid .docx Word file and can be opened in Microsoft Word."
      abort("Aborted: Pandoc conversion from DOCX to HTML failed. #{e}")
    end

    tables_removed = html.scan(/(Here has been a table that has been removed because tables currently aren’t supported by delvyDialogue.)/).length
    images_removed = html.scan(/(Here has been an image that has been removed because images currently aren’t supported by delvyDialogue.)/).length

    if tables_removed > 0 or images_removed > 0
      ActionCable.server.broadcast "import_original_text_user_#{original_text_import.author.id}",
                                   status: "IMPORT_WARNINGS",
                                   tables_removed: tables_removed,
                                   images_removed: images_removed
    end

    # TODO: Get rid of this manually added header from Pandoc
    # TODO: i.e. find out why `PandocRuby.new(…, standalone: true` doesn't work
    full_html = <<-HTML
<!DOCTYPE html>		
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">		
<head>		
  <meta charset="utf-8" />		
  <meta name="generator" content="pandoc" />		
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />		
  <title>inmedio_0_Mainstream_narratives_concerning_NATO_and_EU_enlargement</title>		
  <style>		
    code{white-space: pre-wrap;}		
    span.smallcaps{font-variant: small-caps;}		
    span.underline{text-decoration: underline;}		
    div.column{display: inline-block; vertical-align: top; width: 50%;}		
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}		
    ul.task-list{list-style: none;}		
  </style>		
  <!--[if lt IE 9]>		
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>		
  <![endif]-->		
</head>		
<body>
#{html}
</body>		
</html>
    HTML

    # Import the text from the converted HTML
    group_id = original_text_import.author.active_group.id
    original_text_id = ApplicationController.helpers.import_html(full_html, original_text_import.title, group_id)

    ActionCable.server.broadcast "import_original_text_user_#{original_text_import.author.id}",
                                 status: "IMPORT_SUCCEEDED",
                                 original_text_id: original_text_id
  end
end
