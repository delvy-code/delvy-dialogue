--- Pandoc filter that removes all images completely and replaces them with a
--- corresponding removal notice
function Para(elem)
    if elem.content[1].t == "Image" then
        return pandoc.Para{pandoc.Code("(Here has been an image that has been removed because images currently aren’t supported by delvyDialogue.)")}
    end
end
