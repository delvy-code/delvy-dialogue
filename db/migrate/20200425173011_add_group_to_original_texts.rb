class AddGroupToOriginalTexts < ActiveRecord::Migration[5.2]
  def change
    add_reference :original_texts, :group, foreign_key: true
  end
end
