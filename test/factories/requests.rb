FactoryBot.define do
  factory :request do
    reason { 'This is the reason for the requested change' }
    content { 'Another <strong>bold</strong> content for the heading' }
    is_approved { true }
    author { build(:user1) }
    content_unit
  end

  factory :change_request, parent: :request, class: 'ChangeRequest', aliases: [:change_request_without_ratings] do
    factory :change_request_with_ratings, parent: :request, class: 'ChangeRequest' do
      transient do
        red_ratings_count { 0 }
        orange_ratings_count { 0 }
        yellow_ratings_count { 1 }
        green_ratings_count { 0 }
      end

      after(:create) do |change_request, evaluator|
        create_list(:red_rating, evaluator.red_ratings_count, request: change_request)
        create_list(:orange_rating, evaluator.orange_ratings_count, request: change_request)
        create_list(:yellow_rating, evaluator.yellow_ratings_count, request: change_request)
        create_list(:green_rating, evaluator.green_ratings_count, request: change_request)
      end
    end
  end
end
