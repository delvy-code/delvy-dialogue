class DeleteRatingsOnOwnedRequests < ActiveRecord::Migration[5.2]
  def up
    Rating.find_each do |rating|
      if rating.request.author_id == rating.author_id
        rating.delete
      end
    end
  end
end
