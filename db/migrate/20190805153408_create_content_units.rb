class CreateContentUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :content_units do |t|
      t.integer :starts_at
      t.integer :ends_at
      t.boolean :should_render

      t.belongs_to :original_text, index: true, foreign_key: true

      t.timestamps
    end
  end
end
