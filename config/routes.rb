Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'welcome/index'
  root 'welcome#index'

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'

  # @todo #nicetohave: mount in production as well, only for admin users https://github.com/mperham/sidekiq/wiki/Devise
  unless  Rails.env.production?
    require 'sidekiq/web'
    mount Sidekiq::Web => '/sidekiq'
  end

  resources :users

  resources :original_texts, shallow: true do
    resources :content_units
  end
  resources :content_units, shallow: true do
    resources :requests
  end
  resources :requests do
    resources :ratings
    resources :comments
  end
  resources :groups do
    member do
      get 'join'
      post 'invite-user'
    end
  end

end
