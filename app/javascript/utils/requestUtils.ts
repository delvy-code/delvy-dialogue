export function isChangeRequest(request: IRequest): request is IChangeRequest {
    return request.class_name === "ChangeRequest";
}

export function isAgreementRequest(request: IRequest): request is IAgreementRequest {
    return request.class_name === "AgreementRequest";
}

export function isOriginalRequest(request: IRequest): request is IOriginalRequest {
    return request.class_name === "OriginalRequest";
}
