class Rating < ApplicationRecord
  enum readable_value: %i[red orange yellow green]

  belongs_to :request
  belongs_to :author, class_name: 'User'

  validate :author_cant_be_request_author;

  scope :is_red, -> { where(value: readable_values[:red]) }
  scope :is_orange, -> { where(value: readable_values[:orange]) }
  scope :is_yellow, -> { where(value: readable_values[:yellow]) }
  scope :is_green, -> { where(value: readable_values[:green]) }

  def author_cant_be_request_author
    if request.author == author
      errors.add(:author, "can't be the same as the request author since rating your own request is not allowed");
    end
  end

end
