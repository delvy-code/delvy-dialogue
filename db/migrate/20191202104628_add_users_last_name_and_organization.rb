class AddUsersLastNameAndOrganization < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |u|
      u.string :last_name
      u.string :organization
    end
  end
end
