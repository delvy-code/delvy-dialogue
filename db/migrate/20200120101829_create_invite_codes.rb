class CreateInviteCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :invite_codes do |t|
      t.string :code
      t.boolean :used

      t.timestamps
    end
  end
end
