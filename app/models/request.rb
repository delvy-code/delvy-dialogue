class Request < ApplicationRecord
  belongs_to :content_unit
  belongs_to :author, class_name: 'User', optional: true
  has_many :ratings, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_and_belongs_to_many :agreement_requests, join_table: 'agreement_requests_requests'

  def class_name
    self.class.name
  end

  def attributes
    super.merge(
      worst_rating_value: worst_rating_value
    )
  end

  scope :has_ratings, -> { includes(:ratings).where.not(ratings: { id: nil }) }
  scope :has_no_ratings, -> { includes(:ratings).where(ratings: { id: nil }) }

  def worst_rating_value
    return nil if ratings.empty?

    (ratings.sort_by &:value).first.value
  end

  # Declares an array of relative amount of ratings (from worst to best) which
  # can be used for sorting requests via the sort_by() function
  # Example: 1 red, 2 orange, 0 yellow and 2 green ratings => [0.2, 0.4, 0, 0.4]
  def sorting_array
    return [0.0, 0.0, 0.0, 0.0] if ratings.empty?

    [
      Rating.is_red,
      Rating.is_orange,
      Rating.is_yellow,
      Rating.is_green
    ].map! do |rating_category|
      ratings.merge(rating_category).size / ratings.size.to_f
    end
  end

  # Limit the scopes defined above to the parent model
  def self.inherited(mod)
    super
    class << mod
      undef :has_ratings
      undef :has_no_ratings
    end
  end
end
