FactoryBot.define do
  factory :user, aliases: [:author] do
    is_moderator { false }
    constructive_consent { '1' }
    privacy_consent { '1' }
    personal_information_consent { '1' }
  end

  factory :user1, parent: :user do
    first_name { 'Cecilia' }
    last_name { 'Smith' }
    email { 'cecilia@example.org' }
    password { 'secret' }
  end

  factory :user2, parent: :user do
    first_name { 'Tom' }
    last_name { 'Mueller' }
    email { 'tom@example.org' }
    password { 'secret' }
  end
end
