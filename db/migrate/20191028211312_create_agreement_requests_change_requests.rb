class CreateAgreementRequestsChangeRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :agreement_change_requests, id: false do |t|
      t.belongs_to :agreement_request
      t.belongs_to :change_request
    end
  end
end
