class CommentsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  # GET /requests/:request_id/comments
  def index
    @comments = Comment.includes(:author)
                       .where(request_id: params[:request_id])
    render json: @comments.to_json(include: [:author])
  end

  # POST /requests/:request_id/comments
  def create
    @request = Request.find(params[:request_id])
    @author = current_user
    request_params_copy = request_params
    # TODO Only set "is_approved" to true if review mode (to be implemented) is disabled
    request_params_copy[:is_approved] = true
    request_params_copy[:author_id] = @author.id
    request_params_copy[:request_id] = @request.id
    @comment = Comment.create!(request_params_copy)
    render json: @comment, status: :created
  end

  private

  def request_params
    # whitelist params
    params.require(:comment).permit(:content)
  end
end
