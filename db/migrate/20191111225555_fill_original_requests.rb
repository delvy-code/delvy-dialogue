class FillOriginalRequests < ActiveRecord::Migration[5.2]
  def up
    ContentUnit.find_each do |content_unit|
      if content_unit.requests.size > 0 && content_unit.requests.none? { |request| request.type == "OriginalRequest" }
        original_request = OriginalRequest.create!(:is_approved => true, :author_id => "", :content_unit_id => content_unit.id, :content =>  content_unit.original_content)
        original_request.created_at = content_unit.created_at
        original_request.save!
      end
    end
  end
end
