require 'test_helper'

class ContentUnitTest < ActiveSupport::TestCase

  class ContentUnitWithoutRequestsTest < ActiveSupport::TestCase
    setup do
      @content_unit = create(:content_unit)
    end

    test 'sorted_requests should be empty if there are no requests yet' do
      assert_empty @content_unit.sorted_requests
    end

    test "best_rated_request should return 'nil' when there are no requests yet" do
      assert_nil @content_unit.best_rated_request
    end

    test 'amount_of_requests should return 0 when there are no requests yet' do
      assert_equal 0, @content_unit.amount_of_requests
    end
  end

  class ContentUnitWithOneRatedAndOneUnratedRequestTest < ActiveSupport::TestCase
    setup do
      @content_unit = create(:content_unit)
      @change_request_without_ratings = create(:change_request_without_ratings,
                                               content_unit: @content_unit)
      @change_request_medium_rated = create(:change_request_with_ratings,
                                            red_ratings_count: 0,
                                            orange_ratings_count: 1,
                                            yellow_ratings_count: 1,
                                            green_ratings_count: 2,
                                            content_unit: @content_unit)
    end

    test 'sorted_requests should return the requests in the correct order' do
      assert_equal [@change_request_medium_rated,
                    @change_request_without_ratings],
                   @content_unit.sorted_requests
    end

    test 'best_rated_request should return the best rated request' do
      assert_equal @change_request_medium_rated, @content_unit.best_rated_request
    end

    test 'amount_of_requests should return the number of existing requests' do
      assert_equal 2, @content_unit.amount_of_requests
    end
  end

  class ContentUnitWithOnlyUnratedRequestsTest < ActiveSupport::TestCase
    setup do
      @content_unit = create(:content_unit)
      @first_change_request_without_ratings = create(:change_request_without_ratings,
                                                     content_unit: @content_unit)
      @second_change_request_without_ratings = create(:change_request_without_ratings,
                                                      content_unit: @content_unit)
    end

    test 'sorted_requests should return the requests in the correct order' do
      assert_equal [@first_change_request_without_ratings,
                    @second_change_request_without_ratings],
                   @content_unit.sorted_requests
    end

    test 'best_rated_request should return the best rated request' do
      assert_equal @first_change_request_without_ratings, @content_unit.best_rated_request
    end

    test 'amount_of_requests should return the number of existing requests' do
      assert_equal 2, @content_unit.amount_of_requests
    end
  end

  class ContentUnitWithOneVetoRatedAndOneUnratedRequestTest < ActiveSupport::TestCase
    setup do
      @content_unit = create(:content_unit)
      @change_request_without_ratings = create(:change_request_without_ratings,
                                               content_unit: @content_unit)
      @change_request_with_veto_rating = create(:change_request_with_ratings,
                                                red_ratings_count: 1,
                                                orange_ratings_count: 0,
                                                yellow_ratings_count: 0,
                                                green_ratings_count: 0,
                                                content_unit: @content_unit)
    end

    test 'sorted_requests should return the requests in the correct order' do
      assert_equal [@change_request_without_ratings,
                    @change_request_with_veto_rating],
                   @content_unit.sorted_requests
    end

    test 'best_rated_request should return the best rated request' do
      assert_equal @change_request_without_ratings, @content_unit.best_rated_request
    end

    test 'amount_of_requests should return the number of existing requests' do
      assert_equal 2, @content_unit.amount_of_requests
    end
  end

  class ContentUnitWithMultipleRatedRequestsTest < ActiveSupport::TestCase
    setup do
      @content_unit = create(:content_unit)
      @change_request_without_ratings = create(:change_request_without_ratings,
                                               content_unit: @content_unit)
      @change_request_medium_rated = create(:change_request_with_ratings,
                                            red_ratings_count: 0,
                                            orange_ratings_count: 1,
                                            yellow_ratings_count: 1,
                                            green_ratings_count: 2,
                                            content_unit: @content_unit)
      @change_request_well_rated = create(:change_request_with_ratings,
                                          red_ratings_count: 0,
                                          orange_ratings_count: 0,
                                          yellow_ratings_count: 1,
                                          green_ratings_count: 1,
                                          content_unit: @content_unit)
      @change_request_with_veto_rating = create(:change_request_with_ratings,
                                                red_ratings_count: 1,
                                                orange_ratings_count: 0,
                                                yellow_ratings_count: 0,
                                                green_ratings_count: 0,
                                                content_unit: @content_unit)
    end

    test 'sorted_requests should return the requests in the correct order' do
      assert_equal [@change_request_well_rated,
                    @change_request_medium_rated,
                    @change_request_without_ratings,
                    @change_request_with_veto_rating],
                   @content_unit.sorted_requests
    end

    test 'best_rated_request should return the best rated request' do
      assert_equal @change_request_well_rated, @content_unit.best_rated_request
    end

    test 'amount_of_requests should return the number of existing requests' do
      assert_equal 4, @content_unit.amount_of_requests
    end
  end

  class ContentUnitWithRequestsWithEqualRelativeRatingsTest < ActiveSupport::TestCase
    setup do
      @content_unit = create(:content_unit)
      @change_request_medium_rated_few_ratings = create(:change_request_with_ratings,
                                                        red_ratings_count: 0,
                                                        orange_ratings_count: 1,
                                                        yellow_ratings_count: 1,
                                                        green_ratings_count: 2,
                                                        content_unit: @content_unit)
      @change_request_medium_rated_many_ratings = create(:change_request_with_ratings,
                                                         red_ratings_count: 0,
                                                         orange_ratings_count: 2,
                                                         yellow_ratings_count: 2,
                                                         green_ratings_count: 4,
                                                         content_unit: @content_unit)
    end

    test 'sorted_requests should return the requests in the correct order' do
      assert_equal [@change_request_medium_rated_many_ratings,
                    @change_request_medium_rated_few_ratings],
                   @content_unit.sorted_requests
    end

    test 'best_rated_request should return the best rated request' do
      assert_equal @change_request_medium_rated_many_ratings, @content_unit.best_rated_request
    end

    test 'amount_of_requests should return the number of existing requests' do
      assert_equal 2, @content_unit.amount_of_requests
    end
  end

  class ContentUnitWithRequestsWithEqualAbsoluteRatingsTest < ActiveSupport::TestCase
    setup do
      @content_unit = create(:content_unit)
      @change_request_created_first = create(:change_request_with_ratings,
                                             red_ratings_count: 0,
                                             orange_ratings_count: 1,
                                             yellow_ratings_count: 1,
                                             green_ratings_count: 2,
                                             content_unit: @content_unit)
      @change_request_created_second = create(:change_request_with_ratings,
                                              red_ratings_count: 0,
                                              orange_ratings_count: 1,
                                              yellow_ratings_count: 1,
                                              green_ratings_count: 2,
                                              content_unit: @content_unit)
    end

    test 'sorted_requests should return the requests in the correct order' do
      assert_equal [@change_request_created_first,
                    @change_request_created_second],
                   @content_unit.sorted_requests
    end

    test 'best_rated_request should return the best rated request' do
      assert_equal @change_request_created_first, @content_unit.best_rated_request
    end

    test 'amount_of_requests should return the number of existing requests' do
      assert_equal 2, @content_unit.amount_of_requests
    end
  end

end
