class RenameGroupsAdminsIndex < ActiveRecord::Migration[5.2]
  def change
    rename_index :groups_owners, 'by_group_and_admin', 'by_group_and_owner'
  end
end
