interface IComment extends IWithTimestamps {
    id?: number;
    content?: string;
    is_approved?: boolean;
    author?: IUser;
    request?: IRequest;
}