class ChangeUserNameFieldToBeUnique < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      execute 'UPDATE users SET name = email WHERE name IS NULL'
      t.index :name, unique: true
    end
  end
end
