class AddHiddenToOriginalTexts < ActiveRecord::Migration[5.2]
  def change
    add_column :original_texts, :hidden, :boolean, default: false
  end
end
