namespace :invite_code do
  desc 'Generate an invite code necessary for adding new user to existing group.
  Parameters:
    - group_id: id of the group the invite code should be for (mandatory)'
  task :generate_code, [:group_id] => :environment do |_, args_rake|
    _, *args = ARGV

    group_id = args_rake.group_id || args.first
    abort("Aborted: Need group id as parameter") unless group_id
    puts(format_output(ApplicationController.helpers.generate_invite_codes(group_id)))
    exit
  end

  desc 'Generate a number of invite codes necessary for adding new users to existing group.
  Parameters:
    - group_id: id of the group the invite code should be for (mandatory)
    - amount: number of codes to generate (mandatory)'
  task :generate_codes, [:group_id, :amount] => :environment do |_, args_rake|
    _, *args = ARGV

    group_id = args_rake.group_id || args.first
    amount = args_rake.amount || args.second.to_i
    abort("Aborted: Need group id and amount as parameters") unless amount && group_id
    puts(format_output(ApplicationController.helpers.generate_invite_codes(group_id, amount)))
    exit
  end

  def format_output(arr)
    output = "Generated #{arr.size} single-use invite code(s): \n"
    arr.each { |inviteCode| output += "#{inviteCode.code.to_s}\n" }
    output
  end
end
