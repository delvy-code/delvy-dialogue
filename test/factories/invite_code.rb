FactoryBot.define do
  factory :invite_code do
    group
  end

  factory :invalid_invite_code, parent: :invite_code do
    code { 'already_used_code' }
    used { true }
  end

  factory :valid_invite_code, parent: :invite_code do
    code { 'valid_code' }
    used { false }
  end
end