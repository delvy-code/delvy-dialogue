class AddUniqueIndexToGroupsMembers < ActiveRecord::Migration[5.2]
  def change
    add_index :groups_members, %i[group_id user_id], unique: true, name: 'by_group_and_member'
  end
end
