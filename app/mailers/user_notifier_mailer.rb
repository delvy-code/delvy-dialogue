class UserNotifierMailer < ApplicationMailer
  default from: "team@#{Rails.application.config.domain}"

  def send_invite_email(email, from_user,  group, invite_code)
    @group = group
    @from_user = from_user
    @url = build_invite_url(group, invite_code)
    mail( to: email,
          subject: "You have been invited to #{group.name} on delvyDialogue!" )
  end

  def build_invite_url(group, invite_code)
    join_group_url(group, invite_code: invite_code.code)
  end
end
