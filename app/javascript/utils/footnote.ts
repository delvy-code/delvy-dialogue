const matchAll = require('string.prototype.matchall');

export function placeFootnotesToTheEndOfHtmlString(html: string): string {
    const regex = new RegExp(/<footnote>(.*?)<\/footnote>/g);

    const footnotes = Array.from(matchAll(html, regex), (m: any) => m[1]);
    if (footnotes.length === 0) {
        return html;
    }

    let footnoteIndex = 0;
    const footnoteToFootnoteMark = (): string => {
        footnoteIndex += 1;
        return `<sup>${footnoteIndex}</sup>`;
    };
    let contentWithFootnotesAppended = html.replace(regex, footnoteToFootnoteMark);

    contentWithFootnotesAppended += '\n<ol class="list-group list-group-flush footnote-list pl-3 mt-2 small">';
    footnotes.forEach((footnote, index) => {
        contentWithFootnotesAppended += `\n  <li>${footnote}</li>`;
    });
    contentWithFootnotesAppended += "\n</ol>";

    return contentWithFootnotesAppended;
}
