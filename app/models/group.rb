class Group < ApplicationRecord
  has_and_belongs_to_many :members, class_name: "User", join_table: 'groups_members', distinct: true
  has_and_belongs_to_many :owners, class_name: "User", join_table: 'groups_owners', distinct: true

  has_many :original_texts

  has_many :original_texts, dependent: :destroy
  has_many :invite_codes, dependent: :destroy
end
