class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.string :content
      t.boolean :is_approved

      t.belongs_to :author, index: true, foreign_key: {to_table: :users}
      t.belongs_to :request, index: true, foreign_key: true

      t.timestamps
    end
  end
end
