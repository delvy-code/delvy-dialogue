class AddXpathIdentifierToContentUnits < ActiveRecord::Migration[5.2]
  def change
    change_table :content_units do |t|
      t.string :xpath_identifier
    end
  end
end
