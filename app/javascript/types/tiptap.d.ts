// Taken from https://gist.github.com/Chris2011/ae357032eca061995a80c05087f6c5d5
declare module "tiptap" {
    import Vue from "vue";
    export class Editor {
        public constructor({});

        public setOptions({}): void;
        public setContent(content: string, immediate?: boolean): void;
        public destroy(): void;
    }

    export class EditorMenuBar extends Vue {}

    export class EditorContent extends Vue {}

    export class Extension extends Vue {}

    export class Node extends Vue {}

    export class Text extends Vue {}

    export class Doc extends Vue {}

    export class Paragraph extends Vue {}
}
