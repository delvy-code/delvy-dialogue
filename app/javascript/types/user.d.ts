interface IUser extends IWithTimestamps {
    id: number;
    email: string;
    first_name: string;
    last_name: string;
    organization: string;
    is_moderator: boolean;
}