class CreateDefaultGroupsForUsers < ActiveRecord::Migration[5.2]
  include GroupsHelper

  def change
    User.all.each do |u|
      Group.create!(name: ApplicationController.helpers.default_group_name_for_user(u), members: [u], admins: [u])
    end
  end
end
