/* Taken from https://vuejs.org/v2/guide/typescript.html */

// 1. Make sure to import 'vue' before declaring augmented types
import Vue from "vue";

// 2. Specify a file with the types you want to augment
//    Vue has the constructor type in types/vue.d.ts
declare module "vue/types/vue" {
    // 3. Declare augmentation for Vue
    interface Vue {
        // TODO: Maybe extend the typing even further
        $cable: any;
    }
}

// TODO: Somehow this doesn’t work -- check why it is the case
/*
declare module "vue/types/options" {
    interface ComponentOptions<V extends Vue> {
        // TODO: Maybe extend the typing even further
        i18n?: any[];
    }
}
*/