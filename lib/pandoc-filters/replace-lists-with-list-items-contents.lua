--- Pandoc filter that replaces all BulletList and OrderedList elements with
--- paragraphs of their list items
-- TODO: Make this recursive, i. e. make multi-level lists work properly
function flattenToListItems(elem)
    paragraphs = {}
    paragraph_index = 1
    for i, item in ipairs(elem.content) do
        local first = item[1]
        local second = item[2]
        local third = item[3]
        local fourth = item[4]

        if first and first.t == 'Para' then
            paragraphs[paragraph_index] = pandoc.Para(first.content)
            paragraph_index = paragraph_index + 1
        end

        if second and second.t == 'Para' then
            paragraphs[paragraph_index] = pandoc.Para(second.content)
            paragraph_index = paragraph_index + 1
        end

        if third and third.t == 'Para' then
            paragraphs[paragraph_index] = pandoc.Para(third.content)
            paragraph_index = paragraph_index + 1
        end

        if fourth and fourth.t == 'Para' then
            paragraphs[paragraph_index] = pandoc.Para(fourth.content)
            paragraph_index = paragraph_index + 1
        end
    end
    return pandoc.List:new(paragraphs)
end

return {{
    BulletList = flattenToListItems,
    OrderedList = flattenToListItems
}}
