class RenameGroupsAdminsToGroupsOwnersTable < ActiveRecord::Migration[5.2]
  def change
    rename_table :groups_admins, :groups_owners
  end
end
