ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require 'bundler/setup' # Set up gems listed in the Gemfile.
# FIXME Commented out because of https://youtrack.jetbrains.com/issue/RUBY-20684
# require 'bootsnap/setup' # Speed up boot time by caching expensive operations.
