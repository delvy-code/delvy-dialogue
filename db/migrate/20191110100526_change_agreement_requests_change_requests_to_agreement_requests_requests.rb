class ChangeAgreementRequestsChangeRequestsToAgreementRequestsRequests < ActiveRecord::Migration[5.2]
  def change
    rename_column :agreement_change_requests, :change_request_id, :request_id
    rename_table :agreement_change_requests, :agreement_requests_requests
  end
end
