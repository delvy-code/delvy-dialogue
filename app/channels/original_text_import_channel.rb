class OriginalTextImportChannel < ApplicationCable::Channel
  def subscribed
    stream_from "import_original_text_user_#{current_user.id}"
  end
end
