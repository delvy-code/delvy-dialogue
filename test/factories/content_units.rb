FactoryBot.define do
  factory :content_unit do
    starts_at { 3 }
    ends_at { 47 }
    should_render { true }
    xpath_identifier { '//html/body/p[position() = 1]' }
    original_content { 'Consequently, there is a greater chance that this particular type of “blind spot” can be avoided.' }
    original_text
  end
end
