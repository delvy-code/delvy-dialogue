--- Pandoc filter that removes all tables completely and replaces them with a
--- corresponding removal notice
function Table(elem)
    return pandoc.Para{pandoc.Code("(Here has been a table that has been removed because tables currently aren’t supported by delvyDialogue.)")}
end
