class AddPriorityToOriginalTexts < ActiveRecord::Migration[5.2]
  def change
    add_column :original_texts, :sorting_priority, :integer, default: 0
  end
end
