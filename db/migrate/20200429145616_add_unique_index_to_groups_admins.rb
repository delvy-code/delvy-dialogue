class AddUniqueIndexToGroupsAdmins < ActiveRecord::Migration[5.2]
  def change
    add_index :groups_admins, %i[group_id user_id], unique: true, name: 'by_group_and_admin'
  end
end
