class AddOriginalToContentUnit < ActiveRecord::Migration[5.2]
  def change
    change_table :content_units do |t|
      t.string :original_content
    end
  end
end
