import moment from "moment";

export function makeReadableDatetime(momentParseableDate: any) {
    return moment(momentParseableDate).format("MMMM Do YYYY, hh:mm:ss");
}

export function ago(momentParseableDate: any) {
    return moment(momentParseableDate).fromNow();
}
