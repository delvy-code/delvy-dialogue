class CreateOriginalTexts < ActiveRecord::Migration[5.2]
  def change
    create_table :original_texts do |t|
      t.string :body

      t.timestamps
    end
  end
end
