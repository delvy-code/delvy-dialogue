class CreateOriginalTextImports < ActiveRecord::Migration[5.2]
  def change
    create_table :original_text_imports do |t|
      t.string :title

      t.belongs_to :original_text, index: true, foreign_key: true

      t.timestamps
    end
  end
end
