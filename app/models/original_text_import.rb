class OriginalTextImport < ApplicationRecord
  has_one_attached :original_file
  belongs_to :author, class_name: 'User'
  belongs_to :original_text, optional: true
end
