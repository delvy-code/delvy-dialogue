class AddBelongsToAuthorToOriginalTextImports < ActiveRecord::Migration[5.2]
  def change
    change_table :original_text_imports do |u|
      u.belongs_to :author, index: true, foreign_key: {to_table: :users}
    end
  end
end
