namespace :export_updated_text do
  desc "Exports a given OriginalText using the currently best rated request (if existing) for each of the ContentUnit's texts."
  task :to_pandoc_markdown => :environment do
    _, *args = ARGV
    title = args.first
    abort("Aborted: Need original text title as parameter") unless title
    original_text = OriginalText.where(title: title)
                        .first
    abort("Aborted: Original text with specified title could not be found") unless original_text

    content_units_by_nodes = original_text.content_units.sort_by { |content_unit|
      content_unit.id
    }.group_by { |content_unit| content_unit.xpath_identifier }

    current_footnote_index = 1
    footnotes = []

    markdown_string = ""

    previous_xpath_identifier = nil

    content_units_by_nodes.each do |xpath_identifier, content_units|
      node_string = ""

      if xpath_identifier != previous_xpath_identifier
        node_string += "\n"
      end

      content_units.each do |content_unit|
        if content_unit.best_rated_request
          content = content_unit.best_rated_request.content
          content_html_fragment = Nokogiri::HTML::fragment(content, 'UTF-8')
          content_with_replaced_footnotes, content_footnotes = extract_footnotes_from_content content_html_fragment, current_footnote_index
          if content_footnotes
            footnotes = footnotes.concat content_footnotes
            current_footnote_index += content_footnotes.size
          end
          node_string += content_with_replaced_footnotes.inner_html += " "
        else
          content = content_unit.original_content
          content_html_fragment = Nokogiri::HTML::fragment(content, 'UTF-8')
          content_with_replaced_footnotes, content_footnotes = extract_footnotes_from_content content_html_fragment, current_footnote_index
          if content_footnotes
            footnotes = footnotes.concat content_footnotes
            current_footnote_index += content_footnotes.size
          end
          node_string += content_with_replaced_footnotes.inner_html += " "
        end
      end

      if xpath_identifier != previous_xpath_identifier
        markdown_string += ReverseMarkdown.convert node_string
        markdown_string += "\n"
        markdown_string += "\n"
      end

      previous_xpath_identifier = xpath_identifier
    end

    if footnotes.size > 0
      markdown_string += " \n"
      markdown_string += " \n"

      footnotes.each do |footnote|
        markdown_string += ReverseMarkdown.convert footnote
        markdown_string += "\n"
      end
    end

    puts markdown_string
    exit
  end
end

def extract_footnotes_from_content(content_html, starting_index)
  possibly_incremented_index = starting_index
  added_footnotes = []

  content_html.element_children.each do |child_element|
    if child_element.name === "footnote"
      footnote_content = child_element.inner_html

      replaced_footnote_element = "[^" + possibly_incremented_index.to_s + "]"
      footnote_reference_element = "[^" + possibly_incremented_index.to_s + "]: " + footnote_content

      added_footnotes.append footnote_reference_element

      child_element.replace(replaced_footnote_element)

      possibly_incremented_index += 1
    end
  end

  if added_footnotes.size > 0
    return content_html, added_footnotes
  end

  [content_html, nil]
end