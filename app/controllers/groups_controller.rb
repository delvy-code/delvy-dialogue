class GroupsController < ApplicationController
  protect_from_forgery with: :exception
  # make join route accessible without log in to handle redirect to registration page
  before_action :authenticate_user!, except: [:join]
  before_action :set_group
  before_action :check_for_group, only: [:invite_user]

  # POST /groups/:id/invite-user
  def invite_user
    code = ApplicationController.helpers.generate_invite_codes(@group.id, 1)[0]
    email = invite_user_params[:email]
    UserNotifierMailer.send_invite_email(email, current_user, @group, code).deliver_later
  end

  # GET /groups/:id/join
  def join
    code = params[:invite_code]
    invite_code = InviteCode.find_by(code: code)

    # validate code
    if invite_code && invite_code.group_id == @group.id && !invite_code.used
      if user_signed_in?
        @group.members << current_user unless @group.members.include?(current_user)
        invite_code.update(used: true)
        redirect_to root_path, notice: "You’ve been successfully added to #{@group.name}!"
      else
        # User first has to register but will then be added to group
        redirect_to new_user_registration_path(params.permit(:invite_code))
      end
    else
      path = user_signed_in? ? root_path : new_user_registration_path
      redirect_to path, alert: 'Invalid invite link!'
    end
  end

  def invite_user_params
    params.require(:groups_invite_user).permit(:email)
  end

  private
  def set_group
    @group = Group.find(params[:id])
  end

  def check_for_group
    head :unauthorized unless current_user.groups_as_member.include?(@group)
  end
end
