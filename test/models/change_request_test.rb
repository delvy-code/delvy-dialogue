require 'test_helper'

class ChangeRequestTest < ActiveSupport::TestCase

  test 'has_ratings scope should include change_requests that have ratings' do
    change_request = create(:change_request_with_ratings)
    assert_includes Request.has_ratings, change_request
  end

  test "has_ratings scope should not include change_requests that don't have ratings" do
    change_request = create(:change_request_without_ratings)
    assert_not_includes Request.has_ratings, change_request
  end

  test 'has_no_ratings scope should not include change_requests that have ratings' do
    change_request = create(:change_request_with_ratings)
    assert_not_includes Request.has_no_ratings, change_request
  end

  test "has_no_ratings scope should include change_requests that don't have ratings" do
    change_request = create(:change_request_without_ratings)
    assert_includes Request.has_no_ratings, change_request
  end

  test "worst_rating_value should be 'nil' when there are no ratings yet" do
    change_request = create(:change_request_without_ratings)
    assert_nil change_request.worst_rating_value
  end

  test 'worst_rating_value should be correct when there are red ratings' do
    change_request = create(:change_request_with_ratings,
                            red_ratings_count: 1,
                            orange_ratings_count: 1,
                            yellow_ratings_count: 0,
                            green_ratings_count: 1)
    assert_equal Rating.readable_values[:red], change_request.worst_rating_value
  end

  test 'worst_rating_value should be correct when there are no red ratings' do
    change_request = create(:change_request_with_ratings,
                            red_ratings_count: 0,
                            orange_ratings_count: 1,
                            yellow_ratings_count: 0,
                            green_ratings_count: 1)
    assert_equal Rating.readable_values[:orange], change_request.worst_rating_value
  end

  test 'sorting_array should be empty when there are no ratings' do
    change_request = create(:change_request_without_ratings)
    assert_equal [0.0, 0.0, 0.0, 0.0], change_request.sorting_array
  end

  test 'sorting_array should be correct when there are some ratings' do
    change_request = create(:change_request_with_ratings,
                            red_ratings_count: 1,
                            orange_ratings_count: 2,
                            yellow_ratings_count: 0,
                            green_ratings_count: 2)
    assert_equal [0.2, 0.4, 0, 0.4], change_request.sorting_array
  end

end
