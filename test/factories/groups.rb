FactoryBot.define do
  factory :group do
    name { 'Cecilia Smith’s workspace' }
  end
end
