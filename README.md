# **delvy**Dialogue
A collaborative document editor focusing on consensus based discussion and decision making.

![screenshot](./scrennshot_readme.png)

## How to run

### Dependencies

* Node.js + yarn
* Ruby 2.4.6 (preferably using [rbenv](https://github.com/rbenv/rbenv#installation))
* Docker

### Frontend

1. Install the Node.js dependencies:
    ```bash
    yarn install
    ```
1. Run the compilation and live-reloading server:
    ```bash
    ./bin/webpack-dev-server
    ```

### Backend

Make sure that you can run the required Ruby version:
```bash
rbenv install 2.4.6
```

Make sure to have bundler installed:
```bash
gem install bundler
```

1. Install the Ruby requirements from the Gemfile:
    ```bash
    bundle install
    ```
1. Run the Postgres server using Docker:
    ```bash
    docker-compose up
    ```
1. Run the migrations:
    ```bash
    rake db:migrate
    ```
1. Run the Rails server:
    ```bash
    ./bin/rails server
    ```
   
### Importing Documents

Make sure you have pandoc installed:
```bash
brew install pandoc
```

1. Convert a given DOCX file to HTML using Pandoc:
    ```bash
    pandoc -s original_text.docx -t html -o original_text.html
    ```
1. Run the custom Rake task to convert the HTML document to our internal OriginalText/ContentUnit format:
    ```bash
    rake import_original_text:from_html test/fixtures/files/original_text.html "The title"
    ```

You should now have the corresponding ContentUnit and OriginalText records in your database.

### Exporting Documents

Make sure you have pandoc installed:
```bash
brew install pandoc
```

1. Run the custom Rake task to export a given OriginalText including all of its ContentUnits (with possible replacements with best-rated requests) to a Markdown document suitable for Pandoc:
    ```bash
    rake  export_updated_text:to_pandoc_markdown "The OriginalText's title" > original_text_export.md
    ```
1. Convert the resulting Markdown document to DOCX using Pandoc: 
    ```bash
    pandoc -s original_text_export.md --from markdown-markdown_in_html_blocks+raw_html -t docx -o original_text_export_pandoc_converted.docx
    ```

### Generating Invite Codes

Run the rake task (passing in the number of codes you want to generate) and copy the codes that will be printed in the terminal:
```bash
rake invite_code:generate_codes 15
```

### Local testing smtp server

MailCatcher can be used to preview email sending locally at [localhost:1080](http://127.0.0.1:1080)
```bash
gem install mailcatcher
mailcatcher
```

### Tests

(Install Node.js and Gemfile requirements as described in "Frontend" and "Backend")

1. Run the Testing Postgres server using Docker:
   ```bash
   docker-compose up
   ```
1. Run the tests:
   ```bash
   rake test
   ```

## How to deploy

The deployment is done through Heroku and triggered via GitLab CI (see `.gitlab-ci.yml`).

If you did add new migrations (or did other changes that require calling specific commands), you can use the Heroku CLI to do so:

```bash
heroku run rails db:migrate
``` 
