module InviteCodesHelper
  # Creates invite codes for a group.
  # @param [integer] amount of codes to generate
  # @param [string] group_id of the group the invite codes will be for
  # @return [Array<InviteCode>] invite codes
  def generate_invite_codes(group_id, amount = 1)
    invite_codes = []
    while invite_codes.size != amount do
      invite_codes = (0...amount).map { SecureRandom.hex(7) }
      invite_codes = invite_codes.uniq
    end
    invite_codes.map do |invite_code|
      InviteCode.create!(code: invite_code, used: false, group_id: group_id)
    end
  end

end
