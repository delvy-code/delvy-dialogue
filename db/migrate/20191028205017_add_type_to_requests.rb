class AddTypeToRequests < ActiveRecord::Migration[5.2]
  def change
    change_table :requests do |t|
      t.string :type
      execute "UPDATE requests SET type = 'ChangeRequest'"
    end
  end
end
