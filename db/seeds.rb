# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if ENV['DEMO_MODE']
  puts "Creating demo user: test@test.com/password"
  invite_code = SecureRandom.hex(7)
  InviteCode.find_or_create_by!( {code: invite_code, used: false} )
  User.create!  first_name: 'Erika',
                last_name: 'Mustermann',
                email: 'test@test.com',
                password: 'password',
                is_moderator: false,
                organization: 'Beispielfirma',
                sign_up_code: invite_code,
                constructive_consent: '1',
                privacy_consent: '1',
                personal_information_consent: '1'
end
