interface IContentUnit extends IWithTimestamps {
    id: number;
    starts_at: number;
    ends_at: number;
    should_render: boolean;
    original_text_id: number;
    xpath_identifier: string;
    original_content: string;
    amount_of_requests: number;
    best_rated_request?: IRequest;
}