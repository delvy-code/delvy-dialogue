interface ICommentMaybeWithRating {
    comment: IComment;
    rating?: IRating;
}