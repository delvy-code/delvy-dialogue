class ContentUnit < ApplicationRecord
  belongs_to :original_text
  has_many :requests, dependent: :destroy
  has_many :non_original_requests, -> { where({type: ["ChangeRequest", "AgreementRequest"]}) }, class_name: 'Request'

  def attributes
    super.merge(
      best_rated_request: best_rated_request,
      amount_of_requests: amount_of_requests
    )
  end

  def sorted_requests
    return [] if requests.empty?

    rated_requests_sorted
  end

  def best_rated_request
    return nil if requests.empty?

    rated_requests_sorted[0]
  end

  def amount_of_requests
    non_original_requests.size
  end

  private

  def sort_requests(request_a, request_b)

    # If both requests have ratings, compare them accordingly
    if request_a.ratings.size.positive? && request_b.ratings.size.positive?
      # First, compare the relative amounts of ratings (&sorting_array)
      [0, 1, 2, 3].each { |rating_value|
        if request_a.sorting_array[rating_value] > request_b.sorting_array[rating_value]
          return 1
        end
        if request_a.sorting_array[rating_value] < request_b.sorting_array[rating_value]
          return -1
        end
      }
      # If all values have been equal, compare the absolute amount of ratings
      # (the more ratings a request has, the better its rank)
      return -1 if request_a.ratings.size > request_b.ratings.size
      return 1 if request_a.ratings.size < request_b.ratings.size
      # If still all values are equal, the "earliest" request is the best one
      return -1 if request_a.created_at < request_b.created_at
      return 1 if request_a.created_at > request_b.created_at

      # Otherwise (should probably never happen), rate them as being equal
      return 0
    end

    # If one of the requests has no ratings yet, rate Vetos worse
    if request_a.ratings.size.positive? && request_b.ratings.size.zero?
      # If request_a has any Veto ratings, rate it worse
      return 1 if request_a.sorting_array[0] > 0.0

      # Otherwise, any rated request is always better than an unrated one
      return -1
    elsif request_b.ratings.size.positive? && request_a.ratings.size.zero?
      # If request_b has any Veto ratings, rate it worse
      return -1 if request_b.sorting_array[0] > 0.0

      # Otherwise, any rated request is always better than an unrated one
      return 1
    end

    # If none of the requests have ratings, compare them by their creation date
    return -1 if request_a.created_at < request_b.created_at
    return 1 if request_a.created_at > request_b.created_at

    # Otherwise (should probably never happen), rate them as being equal
    0
  end

  def rated_requests_sorted
    return [] if requests.empty?

    # Sort the requests according to their specified sorting order
    requests.sort do |request_a, request_b|
      sort_requests request_a, request_b
    end
  end
end
