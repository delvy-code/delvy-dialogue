require 'test_helper'

class OriginalTextsControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  setup do
    @group = create(:group)
    @owner = create(:user1)
    @group.members << @owner
    @group.owners << @owner
    @original_text = create(:original_text, group_id: @group.id)
    login_as @owner
  end

  test 'should list texts belonging to group' do
    get original_texts_url
    texts = JSON.parse(@response.body)
    assert_equal 1, texts['all'].length
    assert_response :success
  end

  test 'should not list texts not belonging to group' do
    user2 = create(:user2)
    login_as(user2)
    get original_texts_url
    texts = JSON.parse(@response.body)
    assert_equal 0, texts['all'].length
    assert_response :success
  end

  test 'should trigger OriginalTextImport' do
    OriginalTextImport.expects(:create!)
    ImportOriginalTextFromFileJob.expects(:perform_later)
    post original_texts_url, params: { original_text: { title: 'test_title', original_file: nil } }
    assert_response :created
    # TODO: Add test for ImportOriginalTetFromFileJob (#99)
  end

  # TODO: Add test for rendering of an OriginalText (#99)

  test 'should allow an owner to delete a text' do
    text_id = @original_text.id
    delete original_text_url(@original_text)
    assert OriginalText.where(id: text_id).empty?
    assert_response :success
  end

  test 'should not allow an member to delete a text' do
    text_id = @original_text.id
    @group.owners.delete(@owner)
    delete original_text_url(@original_text)
    assert_not OriginalText.where(id: text_id).empty?
    # should be unauthorized
    assert_response 401
  end
end
