require 'test_helper'

class UserTest < ActiveSupport::TestCase
  include GroupsHelper

  test 'should find user for email' do
    user = create(:user1)
    assert user == User.find_record(user.email)
  end

  test 'should be added to group if created without invite code' do
    user = create(:user1)
    assert_equal 1, user.groups_as_member.length
    assert_equal 1, user.groups_as_owner.length
  end

  test 'should create a new default group if created without invite code' do
    before_groups_count = Group.count
    user = create(:user1)
    assert_equal before_groups_count + 1, Group.count
    assert_equal 1, user.groups_as_member[0].members.length
    assert_equal 1, user.groups_as_owner[0].owners.length
    user.groups_as_member.take.name = default_group_name_for_user(user)
  end

  test 'should not create new group if created with invite code' do
    valid_invite_code = create(:valid_invite_code)
    before_groups_count = Group.count
    user = create(:user1, sign_up_code: valid_invite_code.code)
    assert_equal before_groups_count, Group.count
  end

  test 'should join an existing group if created with invite code' do
    valid_invite_code = create(:valid_invite_code)
    user = create(:user1, sign_up_code: valid_invite_code.code)
    assert user.groups_as_member.length > 0
  end

  # TODO: Add test for declining of wrong invite codes (#99)
end
