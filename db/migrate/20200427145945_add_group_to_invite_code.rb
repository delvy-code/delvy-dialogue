class AddGroupToInviteCode < ActiveRecord::Migration[5.2]
  def change
    add_reference :invite_codes, :group, foreign_key: true
  end
end
