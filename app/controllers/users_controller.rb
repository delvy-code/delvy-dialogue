class UsersController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  # GET /users
  def index
    # TODO: as soon as we really support multiple groups we should adjust the view to display the members grouped
    # TODO: should be fixed in the scope of https://gitlab.com/delvy-code/delvy-dialogue/-/issues/91
    @users = current_user.groups_as_member.map(&:members).flatten.uniq
    render json: @users
  end
end
